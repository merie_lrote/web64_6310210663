import logo from './logo.svg';
import './App.css';
import Header from './components/Header'
import Footer from './components/Footer'
import Body from './components/Body'


function App() {
  return (
    <div className="App">
      <Header />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          This is my react page
        </p>
        <a href="https://google.com">
          Google
        </a>
      </header>
      <Body />
      <Footer />
    </div>
  );
}

export default App;
