
import BMIResult from "../components/BMIResult";
import {useState} from "react";

function BMICalPage() {

    const [name, setName] = useState("Merie_Lrote");
    const [bmiResult, setBmiResult] = useState("0");
    const [translateResult, setTranslateResult] = useState("");

    const [height, setHeight] = useState("");
    const [weight, setWeight] = useState("");

    function calculateBMI() {
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h*h);
        setBmiResult(bmi);

        if (bmiResult > 25 ) {
            setTranslateResult("FAT");
        }else {
            setTranslateResult("SLIM");
        }
    }

    return (

        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บคำนวณ BMI
                <hr />

                Name: <input type="text"
                            value={name}
                            onChange={ (e) => {setName(e.target.value); }}
                            /> <br />
                Height: <input type="text"
                            value={height}
                            onChange={ (e) => {setHeight(e.target.value); }} 
                            /> <br />
                weight: <input type="text"
                            value={weight}
                            onChange={ (e) => {setWeight(e.target.value); }} 
                            /> <br />
                
                <button onClick={ () =>{calculateBMI()}} >Calculate </button>
                { bmiResult != 0 &&
                    <div>
                        <hr />

                        ผลการคำนวณ
                        <BMIResult
                            name={name}
                            bmi={bmiResult}
                            result={translateResult}
                            />

                    </div>
                }
            </div>
        </div>
    );
}

export default BMICalPage