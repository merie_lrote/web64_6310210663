
import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AaboutUs (props) {




    return (
        <Box sx={{width:"60%"}}>
        <Paper elevation={3}>
        <div>
            <h2> จัดทำโดย: {props.name} </h2>
            <h3> ติดต่อ 000-0000000 {  props.tel} </h3>
            <h3> address 116/39 kohong { props.address } </h3>
        </div>
        </Paper>
        </Box>
    );
}

export default AaboutUs;