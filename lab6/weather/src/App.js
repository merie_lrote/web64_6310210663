import logo from './logo.svg';
import './App.css';

import { Box, AppBar, Typography, Toolbar, Card } from '@mui/material';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';

import { useEffect, useState } from 'react';
import axios from 'axios';
import ReactWeather, { useOpenWeather } from 'react-open-weather';
import YouTube from 'react-youtube';


function App() {

  const [ temperature, setTemperature ] = useState();

  const weatherAPIBaseUrl = 
       "http://api.openweathermap.org/data/2.5/weather?";
const city = "Songkhla";
const apiKey = "a80236df896d4692833ef703d519da4c";
  // openWeatherMap API Key a80236df896d4692833ef703d519da4c

  const { data, isLoading, errorMessage } = useOpenWeather({
    key: '1123a6d82d26243874424e74d6c216fd',
    lat: '7.20695',
    lon: '100.59732',
    lang: 'en',
    unit: 'metric', // values are (metric, standard, imperial)
  });

  useEffect( () => {
    setTemperature("----");

    axios.get(weatherAPIBaseUrl+"q="+city+"&appid="+apiKey).then ( (response)=> {
      let data = response.data;
      let temp = data.main.temp - 273;
      setTemperature(temp.toFixed(0));
    })
  }
  , [] );

  return (
    <Box sx={{ flexGrow : 1, width : "100%" }}>
      <AppBar position="static">
         <Toolbar>
           <Typography variant='h6'>
             WeatherApp
           </Typography>
         </Toolbar>
       </AppBar>

      <Box sx={{justifyContent : "center", marginTop :"20px", width : "100%", display : "flex"}}>
          <Typography variant="h4">
              Hat Yai weather today
          </Typography>
          </Box>

          <Box sx={{justifyContent : "center", marginTop :"20px", width : "100%", display : "flex"}}>


      <Card sx={{minWidth :275}}>
          <CardContent>
              <Typography sx={{fontSize :14}} color="text.secondary" gutterButtom>
              Hat Yai temperature today
              </Typography>
              <Typography variant="h5" component="div">
                  {temperature}
              </Typography>
              <Typography variant="h5" component="div">
              degrees Celsius
              </Typography>
              
            
          </CardContent>
         
      </Card>
        <ReactWeather
        isLoading={isLoading}
        errorMessage={errorMessage}
        data={data}
        lang="en"
        locationLabel="Munich"
        unitsLabels={{ temperature: 'C', windSpeed: 'Km/h' }}
        showForecast
        />
      
       </Box>
       <YouTube  videoId="86YLFOog4GM" />
    </Box>
  );
}

export default App;
